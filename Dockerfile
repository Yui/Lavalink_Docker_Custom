FROM alpine:3.14 as helper

RUN wget https://github.com/melike2d/lavalink/releases/latest/download/Lavalink.jar -O ./Lavalink.jar

FROM azul/zulu-openjdk:13 as app
# Run as non-root user
RUN groupadd -g 322 lavalink && \
    useradd -r -u 322 -g lavalink lavalink
USER lavalink

WORKDIR /opt/Lavalink

COPY --from=helper Lavalink.jar Lavalink.jar

EXPOSE 2333

ENTRYPOINT ["java", "-Djdk.tls.client.protocols=TLSv1.1,TLSv1.2","-Dnativeloader.os=linux", "-Xmx4G", "-jar", "Lavalink.jar"]